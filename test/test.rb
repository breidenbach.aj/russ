# frozen_string_literal: true

require 'open-uri'
require_relative '../lib/russ'

URI.open('https://feeds.megaphone.fm/amelia') do |f|
  channel = Russ::Channel.from_s(f.read)
  puts channel.title
  channel.items.each do |item|
    puts item.title, item.guid.value
  end
end
