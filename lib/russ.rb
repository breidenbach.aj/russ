# frozen_string_literal: true

require_relative 'russ/russ'

module Russ
  module RussClass
    def initialize(hash)
      @hash = hash
    end

    def method_missing(name, *args, &block)
      sname = name.to_sym

      return nil if @hash.nil?

      return @hash[sname] if @hash.keys.include? sname

      super
    end
  end

  class Guid
    include RussClass
  end

  class Image
    include RussClass
  end

  class Enclosure
    include RussClass
  end

  class Item
    include RussClass

    def enclosure
      Enclosure.new(@hash[:enclosure])
    end

    def guid
      Guid.new(@hash[:guid])
    end
  end

  class Channel
    include RussClass
    def self.from_s(str)
      Channel.new(Russ::Native.from_str(str))
    end

    def image
      Image.new(@hash[:image])
    end

    def items
      @hash[:items].map do |item|
        Item.new(item)
      end
    end
  end
end
