use magnus::{define_module, function, Module};
use rss::Channel;
use std::str::FromStr;

fn channel_from_str(s: String) -> Result<magnus::Value, magnus::Error> {
    let channel = Channel::from_str(&s)
        .map_err(|e| magnus::Error::new(magnus::ExceptionClass::default(), e.to_string()))?;

    serde_magnus::serialize(&channel)
}

#[magnus::init]
fn init() {
    let russ = define_module("Russ").unwrap();
    let native = russ.define_module("Native").unwrap();
    native
        .define_module_function("from_str", function!(channel_from_str, 1))
        .unwrap();
}

#[cfg(test)]
mod tests {
    use super::*;
}
