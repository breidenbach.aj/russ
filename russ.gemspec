# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name = 'russ'
  spec.summary = 'Bindings to rust rss'
  # spec.required_ruby_version = '3.2'
  # spec.target_ruby_version = '3.2.2'
  spec.authors = ['Andrew Breidenbach']
  spec.description = 'Bindings to rust rss'
  spec.version = '0.1.0'
  spec.files = Dir['lib/**/*.rb', 'ext/**/*.{rs,toml,lock,rb}']
  spec.extensions = ['ext/russ/extconf.rb']
  spec.add_dependency 'rb_sys', '~> 0.9.39'
  spec.add_development_dependency 'rake-compiler', '~> 1.2.0'
end
